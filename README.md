# Drupal 7 COD Installation Script


This script installs Drupal 7 COD (conference organizing distribution) on a Debian 9 base image. It was written so that I could evaluate the COD software for running our conference. After evaluation, we determined that it would be best to hire an experienced Drupal admin / designer to do the heavy lifting.

## Do Before Running Script

Set up the url you want to use and point it at the public ip address of the machine your are installing the software on.

## Running the script
> ./install.sh webadmin-email-address web-url database-user databasee-user-pwd database-name

Script prompts for various things. These are they.

* mariadb root password
* Lets encrypt email address
* Let's encrypt for redirect choose 2 (all requests redirected to https).

## Configuring COD via Browser

Site name: YourSite

Site e-mail address: admin@example.com

Site maintenance account:

* Username
* email address
* password

Will need to add the smtp module and put in email address, password, and server address.


## Cautions

Under /admin/reports/status there are warnings for Panelizer which says view modes that should have all fields hidden. **Do not change those fields to hidden!** Doing that breaks those page types and they do not display correctly. It also breaks the functionality of scheduling.