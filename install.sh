#!/bin/bash

set -e
set -u
set -o pipefail

# This script installs all of the necessary software for Drupal 
# Conference Organising Distribution (COD) on a Debian 9 Cloud image
# You will need to have ports 22, 80, and 443 open. A domain name should
# point to the external ip address before running this script.

# usage: install.sh emailaddress domainname dbusername dbuserpwd dbname

emailaddress=$1
domainname=$2
dbusername=$3
dbuserpwd=$4
dbname=$5

sudo apt-get update && sudo apt-get -y upgrade

# Install mariadb

sudo apt -y install mariadb-server mariadb-client 

# Secure mariadb

sudo mysql_secure_installation

# Install php

sudo apt -y install php php-{cli,mysql,json,opcache,xml,mbstring,gd,curl,uploadprogress,zip}

# Install apache2

sudo apt -y install apache2

# Enable rewrite module.

sudo a2enmod rewrite
sudo systemctl restart apache2


# Download Drupal 7 COD

cd /tmp

wget https://ftp.drupal.org/files/projects/cod-7.x-2.0-core.tar.gz

tar xvf cod-7.x-2.0-core.tar.gz

sudo mv cod-7.x-2.0 /var/www/html/drupal

# CKEditer 7.x-1.18 included in COD is insecure. Upgrade to 7.x-1.19 manually.

wget https://ftp.drupal.org/files/projects/ckeditor-7.x-1.19.tar.gz
tar -xzf   ckeditor-7.x-1.19.tar.gz

sudo rm -r /var/www/html/drupal/profiles/cod/modules/contrib/ckeditor 

sudo mv /tmp/ckeditor /var/www/html/drupal/profiles/cod/modules/contrib

sudo chown -R www-data:www-data /var/www/html/drupal

# Install Drupal 7 COD

sudo touch /etc/apache2/sites-available/drupal.conf
sudo chmod a+w /etc/apache2/sites-available/drupal.conf

sudo echo "<VirtualHost *:80>" >> /etc/apache2/sites-available/drupal.conf
sudo echo "    ServerAdmin $emailaddress" >> /etc/apache2/sites-available/drupal.conf
sudo echo "    ServerName $domainname" >> /etc/apache2/sites-available/drupal.conf
sudo echo "    DocumentRoot /var/www/html/drupal" >> /etc/apache2/sites-available/drupal.conf
sudo echo "    <Directory /var/www/html/drupal/>" >> /etc/apache2/sites-available/drupal.conf
sudo echo "        Options Indexes FollowSymLinks" >> /etc/apache2/sites-available/drupal.conf
sudo echo "        AllowOverride All" >> /etc/apache2/sites-available/drupal.conf
sudo echo "        Require all granted" >> /etc/apache2/sites-available/drupal.conf
sudo echo "    </Directory>" >> /etc/apache2/sites-available/drupal.conf
sudo echo "    ErrorLog /var/log/apache2/drupal_error.log" >> /etc/apache2/sites-available/drupal.conf
sudo echo "    CustomLog /var/log/apache2/drupal_access.log combined" >> /etc/apache2/sites-available/drupal.conf
sudo echo "</VirtualHost>" >> /etc/apache2/sites-available/drupal.conf

sudo chmod 644 /etc/apache2/sites-available/drupal.conf

# Enable the sites
sudo ln -s /etc/apache2/sites-available/drupal.conf /etc/apache2/sites-enabled/drupal.conf

# Restart apache2
sudo systemctl restart apache2

# Setup ssl only site using Let's encrypt

sudo apt-get -y install certbot python-certbot-apache

sudo certbot --apache

# Create the database

sudo mysql -e "CREATE DATABASE $dbname CHARACTER SET utf8 COLLATE utf8_general_ci";

sudo mysql -e  "CREATE USER $dbusername@localhost IDENTIFIED BY '$dbuserpwd';"

sudo mysql -e "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES ON $dbname.* TO '$dbusername'@'localhost' IDENTIFIED BY '$dbuserpwd'; "

sudo mysql -e "FLUSH PRIVILEGES;"

# Automatically Backup Database

sudo apt install -y automysqlbackup

# Configure site via browser

echo "Go to https://$domainname and configure the site."
echo "Once that is done secure the site by running: sudo chmod 644 sites/default/settings.php."
